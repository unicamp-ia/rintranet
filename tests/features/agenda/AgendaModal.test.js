import React from 'react';
import { shallow } from 'enzyme';
import { AgendaModal } from '../../../src/features/agenda/AgendaModal';

describe('agenda/AgendaModal', () => {
  it('renders node with correct class name', () => {
    const props = {
      agenda: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <AgendaModal {...props} />
    );

    expect(
      renderedComponent.find('.agenda-agenda-modal').length
    ).toBe(1);
  });
});
