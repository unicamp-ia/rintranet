import React from 'react';
import { shallow } from 'enzyme';
import { BlockArea } from '../../../src/features/agenda/component/BlockArea';

describe('agenda/BlockArea', () => {
  it('renders node with correct class name', () => {
    const props = {
      agenda: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <BlockArea {...props} />
    );

    expect(
      renderedComponent.find('.agenda-block-area').length
    ).toBe(1);
  });
});
