import React from 'react';
import { shallow } from 'enzyme';
import { RoomShow } from '../../../src/features/agenda/RoomShow';

describe('agenda/RoomShow', () => {
  it('renders node with correct class name', () => {
    const props = {
      agenda: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <RoomShow {...props} />
    );

    expect(
      renderedComponent.find('.agenda-room-show').length
    ).toBe(1);
  });
});
