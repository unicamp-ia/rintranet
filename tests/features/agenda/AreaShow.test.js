import React from 'react';
import { shallow } from 'enzyme';
import { AreaShow } from '../../../src/features/agenda/AreaShow';

describe('agenda/AreaShow', () => {
  it('renders node with correct class name', () => {
    const props = {
      agenda: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <AreaShow {...props} />
    );

    expect(
      renderedComponent.find('.agenda-area-show').length
    ).toBe(1);
  });
});
