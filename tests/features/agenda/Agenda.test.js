import React from 'react';
import { shallow } from 'enzyme';
import { Agenda } from '../../../src/features/agenda/Agenda';

describe('agenda/Agenda', () => {
  it('renders node with correct class name', () => {
    const props = {
      agenda: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <Agenda {...props} />
    );

    expect(
      renderedComponent.find('.agenda-agenda').length
    ).toBe(1);
  });
});
