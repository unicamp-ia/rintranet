import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  AGENDA_GET_AREAS_BEGIN,
  AGENDA_GET_AREAS_SUCCESS,
  AGENDA_GET_AREAS_FAILURE,
  AGENDA_GET_AREAS_DISMISS_ERROR,
} from '../../../../src/features/agenda/redux/constants';

import {
  getAreas,
  dismissGetAreasError,
  reducer,
} from '../../../../src/features/agenda/redux/getAreas';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('agenda/redux/getAreas', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when getAreas succeeds', () => {
    const store = mockStore({});

    return store.dispatch(getAreas())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', AGENDA_GET_AREAS_BEGIN);
        expect(actions[1]).toHaveProperty('type', AGENDA_GET_AREAS_SUCCESS);
      });
  });

  it('dispatches failure action when getAreas fails', () => {
    const store = mockStore({});

    return store.dispatch(getAreas({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', AGENDA_GET_AREAS_BEGIN);
        expect(actions[1]).toHaveProperty('type', AGENDA_GET_AREAS_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissGetAreasError', () => {
    const expectedAction = {
      type: AGENDA_GET_AREAS_DISMISS_ERROR,
    };
    expect(dismissGetAreasError()).toEqual(expectedAction);
  });

  it('handles action type AGENDA_GET_AREAS_BEGIN correctly', () => {
    const prevState = { getAreasPending: false };
    const state = reducer(
      prevState,
      { type: AGENDA_GET_AREAS_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getAreasPending).toBe(true);
  });

  it('handles action type AGENDA_GET_AREAS_SUCCESS correctly', () => {
    const prevState = { getAreasPending: true };
    const state = reducer(
      prevState,
      { type: AGENDA_GET_AREAS_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getAreasPending).toBe(false);
  });

  it('handles action type AGENDA_GET_AREAS_FAILURE correctly', () => {
    const prevState = { getAreasPending: true };
    const state = reducer(
      prevState,
      { type: AGENDA_GET_AREAS_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getAreasPending).toBe(false);
    expect(state.getAreasError).toEqual(expect.anything());
  });

  it('handles action type AGENDA_GET_AREAS_DISMISS_ERROR correctly', () => {
    const prevState = { getAreasError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: AGENDA_GET_AREAS_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.getAreasError).toBe(null);
  });
});

