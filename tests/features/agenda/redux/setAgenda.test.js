import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  AGENDA_SET_AGENDA_BEGIN,
  AGENDA_SET_AGENDA_SUCCESS,
  AGENDA_SET_AGENDA_FAILURE,
  AGENDA_SET_AGENDA_DISMISS_ERROR,
} from '../../../../src/features/agenda/redux/constants';

import {
  setAgenda,
  dismissSetAgendaError,
  reducer,
} from '../../../../src/features/agenda/redux/setAgenda';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('agenda/redux/setAgenda', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when setAgenda succeeds', () => {
    const store = mockStore({});

    return store.dispatch(setAgenda())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', AGENDA_SET_AGENDA_BEGIN);
        expect(actions[1]).toHaveProperty('type', AGENDA_SET_AGENDA_SUCCESS);
      });
  });

  it('dispatches failure action when setAgenda fails', () => {
    const store = mockStore({});

    return store.dispatch(setAgenda({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', AGENDA_SET_AGENDA_BEGIN);
        expect(actions[1]).toHaveProperty('type', AGENDA_SET_AGENDA_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissSetAgendaError', () => {
    const expectedAction = {
      type: AGENDA_SET_AGENDA_DISMISS_ERROR,
    };
    expect(dismissSetAgendaError()).toEqual(expectedAction);
  });

  it('handles action type AGENDA_SET_AGENDA_BEGIN correctly', () => {
    const prevState = { setAgendaPending: false };
    const state = reducer(
      prevState,
      { type: AGENDA_SET_AGENDA_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.setAgendaPending).toBe(true);
  });

  it('handles action type AGENDA_SET_AGENDA_SUCCESS correctly', () => {
    const prevState = { setAgendaPending: true };
    const state = reducer(
      prevState,
      { type: AGENDA_SET_AGENDA_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.setAgendaPending).toBe(false);
  });

  it('handles action type AGENDA_SET_AGENDA_FAILURE correctly', () => {
    const prevState = { setAgendaPending: true };
    const state = reducer(
      prevState,
      { type: AGENDA_SET_AGENDA_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.setAgendaPending).toBe(false);
    expect(state.setAgendaError).toEqual(expect.anything());
  });

  it('handles action type AGENDA_SET_AGENDA_DISMISS_ERROR correctly', () => {
    const prevState = { setAgendaError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: AGENDA_SET_AGENDA_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.setAgendaError).toBe(null);
  });
});

