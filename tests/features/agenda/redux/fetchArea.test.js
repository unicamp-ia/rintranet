import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  AGENDA_FETCH_AREA_BEGIN,
  AGENDA_FETCH_AREA_SUCCESS,
  AGENDA_FETCH_AREA_FAILURE,
  AGENDA_FETCH_AREA_DISMISS_ERROR,
} from '../../../../src/features/agenda/redux/constants';

import {
  fetchArea,
  dismissFetchAreaError,
  reducer,
} from '../../../../src/features/agenda/redux/fetchArea';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('agenda/redux/fetchArea', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when fetchArea succeeds', () => {
    const store = mockStore({});

    return store.dispatch(fetchArea())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', AGENDA_FETCH_AREA_BEGIN);
        expect(actions[1]).toHaveProperty('type', AGENDA_FETCH_AREA_SUCCESS);
      });
  });

  it('dispatches failure action when fetchArea fails', () => {
    const store = mockStore({});

    return store.dispatch(fetchArea({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', AGENDA_FETCH_AREA_BEGIN);
        expect(actions[1]).toHaveProperty('type', AGENDA_FETCH_AREA_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissFetchAreaError', () => {
    const expectedAction = {
      type: AGENDA_FETCH_AREA_DISMISS_ERROR,
    };
    expect(dismissFetchAreaError()).toEqual(expectedAction);
  });

  it('handles action type AGENDA_FETCH_AREA_BEGIN correctly', () => {
    const prevState = { fetchAreaPending: false };
    const state = reducer(
      prevState,
      { type: AGENDA_FETCH_AREA_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.fetchAreaPending).toBe(true);
  });

  it('handles action type AGENDA_FETCH_AREA_SUCCESS correctly', () => {
    const prevState = { fetchAreaPending: true };
    const state = reducer(
      prevState,
      { type: AGENDA_FETCH_AREA_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.fetchAreaPending).toBe(false);
  });

  it('handles action type AGENDA_FETCH_AREA_FAILURE correctly', () => {
    const prevState = { fetchAreaPending: true };
    const state = reducer(
      prevState,
      { type: AGENDA_FETCH_AREA_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.fetchAreaPending).toBe(false);
    expect(state.fetchAreaError).toEqual(expect.anything());
  });

  it('handles action type AGENDA_FETCH_AREA_DISMISS_ERROR correctly', () => {
    const prevState = { fetchAreaError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: AGENDA_FETCH_AREA_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.fetchAreaError).toBe(null);
  });
});

