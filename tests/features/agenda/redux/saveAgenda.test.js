import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';

import {
  AGENDA_SAVE_AGENDA_BEGIN,
  AGENDA_SAVE_AGENDA_SUCCESS,
  AGENDA_SAVE_AGENDA_FAILURE,
  AGENDA_SAVE_AGENDA_DISMISS_ERROR,
} from '../../../../src/features/agenda/redux/constants';

import {
  saveAgenda,
  dismissSaveAgendaError,
  reducer,
} from '../../../../src/features/agenda/redux/saveAgenda';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('agenda/redux/saveAgenda', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('dispatches success action when saveAgenda succeeds', () => {
    const store = mockStore({});

    return store.dispatch(saveAgenda())
      .then(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', AGENDA_SAVE_AGENDA_BEGIN);
        expect(actions[1]).toHaveProperty('type', AGENDA_SAVE_AGENDA_SUCCESS);
      });
  });

  it('dispatches failure action when saveAgenda fails', () => {
    const store = mockStore({});

    return store.dispatch(saveAgenda({ error: true }))
      .catch(() => {
        const actions = store.getActions();
        expect(actions[0]).toHaveProperty('type', AGENDA_SAVE_AGENDA_BEGIN);
        expect(actions[1]).toHaveProperty('type', AGENDA_SAVE_AGENDA_FAILURE);
        expect(actions[1]).toHaveProperty('data.error', expect.anything());
      });
  });

  it('returns correct action by dismissSaveAgendaError', () => {
    const expectedAction = {
      type: AGENDA_SAVE_AGENDA_DISMISS_ERROR,
    };
    expect(dismissSaveAgendaError()).toEqual(expectedAction);
  });

  it('handles action type AGENDA_SAVE_AGENDA_BEGIN correctly', () => {
    const prevState = { saveAgendaPending: false };
    const state = reducer(
      prevState,
      { type: AGENDA_SAVE_AGENDA_BEGIN }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.saveAgendaPending).toBe(true);
  });

  it('handles action type AGENDA_SAVE_AGENDA_SUCCESS correctly', () => {
    const prevState = { saveAgendaPending: true };
    const state = reducer(
      prevState,
      { type: AGENDA_SAVE_AGENDA_SUCCESS, data: {} }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.saveAgendaPending).toBe(false);
  });

  it('handles action type AGENDA_SAVE_AGENDA_FAILURE correctly', () => {
    const prevState = { saveAgendaPending: true };
    const state = reducer(
      prevState,
      { type: AGENDA_SAVE_AGENDA_FAILURE, data: { error: new Error('some error') } }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.saveAgendaPending).toBe(false);
    expect(state.saveAgendaError).toEqual(expect.anything());
  });

  it('handles action type AGENDA_SAVE_AGENDA_DISMISS_ERROR correctly', () => {
    const prevState = { saveAgendaError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: AGENDA_SAVE_AGENDA_DISMISS_ERROR }
    );
    expect(state).not.toBe(prevState); // should be immutable
    expect(state.saveAgendaError).toBe(null);
  });
});

