import React from 'react';
import { shallow } from 'enzyme';
import { Sidebar } from '../../../src/features/layout/Sidebar';

describe('layout/Sidebar', () => {
  it('renders node with correct class name', () => {
    const props = {
      layout: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <Sidebar {...props} />
    );

    expect(
      renderedComponent.find('.layout-sidebar').length
    ).toBe(1);
  });
});
