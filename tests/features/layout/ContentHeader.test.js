import React from 'react';
import { shallow } from 'enzyme';
import { ContentHeader } from '../../../src/features/layout/ContentHeader';

describe('layout/ContentHeader', () => {
  it('renders node with correct class name', () => {
    const props = {
      layout: {},
      actions: {},
    };
    const renderedComponent = shallow(
      <ContentHeader {...props} />
    );

    expect(
      renderedComponent.find('.layout-content-header').length
    ).toBe(1);
  });
});
