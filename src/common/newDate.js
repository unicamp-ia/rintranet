import { utcToZonedTime } from 'date-fns-tz'

/**
 *
 * @param date
 * @returns {Date}
 */
export default function newDate(date) {
  let validDate = new Date(date)
  const timeZone = 'America/Noronha'

  validDate = utcToZonedTime(validDate,timeZone)

  return validDate;
}


