import React, { Component } from 'react';
import {Icon} from 'antd'
import PropTypes from 'prop-types'

export default class PageTitle extends Component {
  static contextTypes = {
    router: PropTypes.object.isRequired
  }

  render() {
    return <h2>
      <span onClick={this.context.router.history.goBack}>
        <Icon type="left"/>
        <span className="ml-2 mt-2" >{this.props.title}</span>
      </span>
    </h2>
  }
}
