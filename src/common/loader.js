import React, { Component } from 'react';
import { Spin } from 'antd';
import PropTypes from 'prop-types'

const pad = {
  padding: '20px',
  textAlign: 'center'
}

export default class Loader extends Component {
  static contextTypes = {
    router: PropTypes.object.isRequired
  }



  render() {
    return <div className="loader" style={pad}>
      <Spin tip="Carregando..."/>
    </div>
  }
}
