export default props => {

  if(typeof props.test !== 'undefined'){
    if(props.test){
      return props.children
    } else {
      return false
    }
  }

  if(typeof props.testNot !== 'undefined'){
    console.log(props.testNot)
    if(props.testNot){
      return false
    } else {
      return true
    }
  }
}
