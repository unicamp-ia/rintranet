import {
  HomePage,
} from './';

export default {
  path: '/',
  name: 'Home',
  childRoutes: [
    { path: '/home',
      name: 'Home page',
      component: HomePage,
      isIndex: true,
    },
  ],
};
