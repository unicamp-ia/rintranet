import React, { Component } from 'react';
import { BlockArea } from './component/BlockArea';
import IfNot from '../../common/ifnot'
import If from '../../common/if'
import { Col, Row, Spin } from 'antd';
import axios from 'axios';


export default class DefaultPage extends Component {
  constructor(props) {
    super(props);
    this.state = {areasList: []}
    this.fetchAreas()
  }

  fetchAreas() {
    axios.get(process.env.REACT_APP_API_BASE+'/agenda/area')
      .then(resp => {
        this.setState({areasList: resp.data})
      })

  }

  render() {
    return (
      <div className="agenda-default-page">
        <Row type="flex" gutter={16}>
          <If test={this.state.areasList}>
            {this.state.areasList.map(areaList => (
              <Col key={areaList.id} lg={8} md={12} xs={24}>
                <BlockArea agenda={areaList} />
              </Col>
            ))}
          </If>
          <IfNot test={this.state.areasList}>
            <Col span={24} className="text-center">
              <Spin tip="carregando..." />
            </Col>
          </IfNot>
        </Row>
      </div>
    );
  }
}
