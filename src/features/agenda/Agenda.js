import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import { Modal, Button } from 'antd';

export class Agenda extends Component {
  static propTypes = {
    agenda: PropTypes.object.isRequired,
    selectedAgenda: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
    visible: PropTypes.object.isRequired
  };



  componentWillMount() {
    console.log(this.state)
    this.setState({loading: false})

  }

  handleCancel = () => {
    this.setState({visible: false})
  }

  render() {

    if(this.props.selectedAgenda) {
      this.setState({visible:true})
    }

    return (
      <div className="agenda-agenda">
        agenda
        {/*{this.props.agenda.selectedAgenda}*/}
        <Modal
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={[
            <Button key="back" onClick={this.handleCancel}>
              Voltar
            </Button>,
            <Button key="submit" type="primary" loading={this.state.loading} onClick={this.handleOk}>
              Salvar
            </Button>,
          ]}
        >
          <p>Some contents...</p>
          <p>Some contents...</p>
          <p>Some contents...</p>
          <p>Some contents...</p>
          <p>Some contents...</p>
        </Modal>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    agenda: state.agenda
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Agenda);
