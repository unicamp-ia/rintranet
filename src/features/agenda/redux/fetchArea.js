import {
  AGENDA_FETCH_AREA_BEGIN,
  AGENDA_FETCH_AREA_SUCCESS,
  AGENDA_FETCH_AREA_FAILURE,
  AGENDA_FETCH_AREA_DISMISS_ERROR,
} from './constants';
import axios from 'axios';


// Rekit uses redux-thunk for async actions by default: https://github.com/gaearon/redux-thunk
// If you prefer redux-saga, you can use rekit-plugin-redux-saga: https://github.com/supnate/rekit-plugin-redux-saga
export function fetchArea(areaId, selectedDate = new Date()) {
  return (dispatch) => { // optionally you can have getState as the second argument
    dispatch({
      type: AGENDA_FETCH_AREA_BEGIN,
    });

    // Return a promise so that you could control UI flow without states in the store.
    // For example: after submit a form, you need to redirect the page to another when succeeds or show some errors message if fails.
    // It's hard to use state to manage it, but returning a promise allows you to easily achieve it.
    // e.g.: handleSubmit() { this.props.actions.submitForm(data).then(()=> {}).catch(() => {}); }
    const promise = new Promise((resolve, reject) => {
      // doRequest is a placeholder Promise. You should replace it with your own logic.
      // See the real-word example at:  https://github.com/supnate/rekit/blob/master/src/features/home/redux/fetchRedditReactjsList.js
      // args.error here is only for test coverage purpose.
      const params = {selectedDate: selectedDate}

      const doRequest = axios.get(`${process.env.REACT_APP_API_BASE}/agenda/area/${areaId}`,{params});

      doRequest.then(
        (res) => {
          dispatch({
            type: AGENDA_FETCH_AREA_SUCCESS,
            data: res,
            selectedDate: selectedDate
          });
          resolve(res);
        },
        // Use rejectHandler as the second argument so that render errors won't be caught.
        (err) => {
          dispatch({
            type: AGENDA_FETCH_AREA_FAILURE,
            data: { error: err },
          });
          reject(err);
        },
      );
    });

    return promise;
  };
}

// Async action saves request error by default, this method is used to dismiss the error info.
// If you don't want errors to be saved in Redux store, just ignore this method.
export function dismissFetchAreaError() {
  return {
    type: AGENDA_FETCH_AREA_DISMISS_ERROR,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case AGENDA_FETCH_AREA_BEGIN:
      // Just after a request is sent
      return {
        ...state,
        fetchAreaPending: true,
        fetchAreaError: null,
      };

    case AGENDA_FETCH_AREA_SUCCESS:
      // The request is success
      return {
        ...state,
        currentArea: action.data.data,
        selectedDate: action.selectedDate,
        fetchAreaPending: false,
        fetchAreaError: null,
      };

    case AGENDA_FETCH_AREA_FAILURE:
      // The request is failed
      return {
        ...state,
        fetchAreaPending: false,
        fetchAreaError: action.data.error,
      };

    case AGENDA_FETCH_AREA_DISMISS_ERROR:
      // Dismiss the request failure error
      return {
        ...state,
        fetchAreaError: null,
      };

    default:
      return state;
  }
}
