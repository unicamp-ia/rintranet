import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {fetchArea} from './redux/fetchArea';
import If from '../../common/if'
import Loader from '../../common/loader'
import PageTitle from '../../common/pageTitle'
import { Button, Row, Col, BackTop, DatePicker } from 'antd';
import { RoomShow } from './RoomShow';
import { format, subDays, addDays } from 'date-fns';
import {ptBR} from 'date-fns/locale'
import moment from 'moment'


export class AreaShow extends Component {
  static propTypes = {
    agenda: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };


  componentWillMount() {
    this.props.actions.fetchArea(this.props.match.params.id)
  }

  onChange = (date, dateString) => {
    date = moment(date._d,'DD/MM/YYYY').toDate()
    this.props.actions.fetchArea(this.props.agenda.currentArea.id, date)
  }

  onPrevDate = event => {
    this.props.actions.fetchArea(this.props.agenda.currentArea.id,subDays(this.props.agenda.selectedDate,1))
  }

  onNextDate = event => {
    this.props.actions.fetchArea(this.props.agenda.currentArea.id,addDays(this.props.agenda.selectedDate,1))
  }


  render() {
    const { currentArea, selectedDate, fetchAreaPending } = this.props.agenda
    const dateFormatList = ['DD/MM/YYYY', 'DD/MM/YY'];

    let loaded = false

    if(fetchAreaPending === false && currentArea){
      loaded = true
    }

    return (
      <div className="agenda-area-show">
        <If test={fetchAreaPending}>
          <Loader/>
        </If>
        {loaded === true &&
          <PageTitle title={currentArea.name}/>
        }
        <If test={loaded}>
          <BackTop />
          <Row className="mb-2">

            <Col span={8}>
              <Button icon="left" onClick={this.onPrevDate}/>
              <Button icon="right" onClick={this.onNextDate}/>
            </Col>

            <Col span={8} className="text-center">
              <strong className="today ml-2 mr-2">{format(selectedDate,'d/MM, eeee',{locale:ptBR})}</strong>
            </Col>

            <Col span={8} className="text-right">
              <DatePicker defaultValue={moment(selectedDate)} onChange={this.onChange} format={dateFormatList}/>

            </Col>
          </Row>
          {loaded && currentArea.salas.map(sala =>
            <RoomShow key={sala.id}
                      agenda={this.props.agenda}
                      sala={sala}
                      currentArea={currentArea}/>)}

        </If>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    agenda: state.agenda,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ fetchArea}, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AreaShow);
