import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../redux/actions';
import { Link } from 'react-router-dom';
import { Card } from 'antd';

const {Meta} = Card

export class BlockArea extends Component {

  static propTypes = {
    agenda: PropTypes.object.isRequired,
  };

  render() {


    return (
      <Link to={`agenda/area/${this.props.agenda.id}`} className="agenda-block-area p2">
        <Card
          hoverable
          cover={<img alt={this.props.agenda.name} src="https://www.iar.unicamp.br/wp-content/uploads/2019/10/musica.png"/>}
        >
          <Meta title={this.props.agenda.name}/>
        </Card>
      </Link>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    agenda: state.agenda,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BlockArea);
