import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {setAgenda} from './redux/setAgenda'
import { Collapse, Table, Tag, Button, Icon } from 'antd';
import format from 'date-fns/format'
import newDate from '../../common/newDate'
import { Agenda } from './Agenda';



export class RoomShow extends Component {
  static propTypes = {
    agenda: PropTypes.object.isRequired,
    sala: PropTypes.object.isRequired,
    currentArea: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };


  deleteAgenda = event => {
    console.log(event)
  }

  handleNew = (agenda) => {
    console.log('agenda')
    // const {selectedAgenda} = this.props.agenda

  }


  render() {
    const { Panel } = Collapse;

    const agendas = buildAgendas(this.props.currentArea.start, this.props.currentArea.end, this.props.sala.agendas)

    const columns = [
      {title: 'Período', dataIndex: 'period', key: '1'},
      {title: 'Usuário', dataIndex: 'user', key: '3'},
      {
        title: 'Status',
        dataIndex: 'status',
        key: '2',
        render: agenda => {
          if(agenda){
            return (
              <Tag color='orange'>{agenda.titulo}</Tag>
            );
          } else {
            return <Tag color='green'>LIVRE</Tag>
          }
        }
      },
      {
        title: 'Ação',
        dataIndex: 'action',
        key: 4,
        render: agenda => {
          if(agenda.id){
            return <Button type="danger" onClick={() => this.deleteAgenda()} agenda={agenda}>
              Cancelar <Icon type="close"/>
            </Button>
          } else {
            return <Button type="primary" onClick={() => this.handleNew(agenda)} agenda={agenda}>
              Reservar
              <Icon type="right"/>
            </Button>
          }
        }
      }
    ]

    console.log(this.props)

    return (
      <div className="agenda-room-show">
        <Collapse className="mb-2">
          <Panel header={this.props.sala.nome} key={this.props.sala.id}>
            <Table dataSource={agendas} columns={columns} size="small"/>
            <Agenda agenda={this.props.agenda} selectedAgenda={this.props.selectedAgenda}/>
          </Panel>
        </Collapse>
      </div>
    );
  }
}



/**
 *
 * @param roomStart Date
 * @param roomEnd Date
 * @param agendas
 * @returns {Array}
 */
function buildAgendas(roomStart, roomEnd, agendas){
  let result = []

  roomStart = new Date(roomStart)
  roomEnd = new Date(roomEnd)
  const diff = roomEnd.getHours() - roomStart.getHours()

  for (var i=0;i<=diff; i++){
    const time = i + Number(roomStart.getHours())
    var agenda = agendas.filter( agenda => {
      const start = newDate(agenda.inicio)
      return Number(format(start,'H')) === time
    })

    agenda ? agenda = agenda[0] : agenda = null

    result[i] = {
      key: i,
      period: period(time),
      status: agenda ? agenda : null,
      action: agenda ? agenda : {id: null, inicio: time},
      user: agenda ? agenda.criado_por.nome : 'Livre'
    }
  }

  return result
}

function period(time) {
  if(time.length === 1) {
    time = '0' + time
  }
  const nextTime = Number(time) + 1

  return time + ':00 - ' + nextTime + ':00'
}

/* istanbul ignore next */
function mapStateToProps(state) {
  console.log(state)
  return {
    agenda: state.agenda,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ setAgenda }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RoomShow);
