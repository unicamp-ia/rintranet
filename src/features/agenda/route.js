// This is the JSON way to define React Router rules in a Rekit app.
// Learn more from: http://rekit.js.org/docs/routing.html

import {
  DefaultPage,
  AreaShow,
} from './';

export default {
  path: 'agenda',
  name: 'Agenda',
  childRoutes: [
    { path: 'default-page', name: 'Areas', component: DefaultPage, isIndex: true },
    { path: '/agenda/area/:id', name: 'Area show', component: AreaShow },
  ],
};
