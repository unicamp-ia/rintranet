import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import { Icon, Menu } from 'antd';
import { Link } from 'react-router-dom';

const { SubMenu } = Menu;

export class Sidebar extends Component {
  static propTypes = {
  };

  render() {
    return (
      <div className="layout-sidebar">
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
          <Menu.Item>
            <span>
              <Icon type="home"/>
              <Link to="/home">Home</Link>
            </span>
          </Menu.Item>
          <SubMenu
            key="agenda"
            title={
              <span>
                  <Icon type="calendar" />
                  <span>Agenda</span>
                </span>
            }
          >
            <Menu.Item>
              <Link to="/agenda">Agenda</Link>
            </Menu.Item>
          </SubMenu>
        </Menu>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    layout: state.layout,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sidebar);
