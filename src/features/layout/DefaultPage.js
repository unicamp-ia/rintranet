import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from './redux/actions';
import 'antd/dist/antd.css';
import { Layout } from 'antd'
import { ContentHeader } from './ContentHeader';
import { Sidebar } from './Sidebar'

const { Content, Sider } = Layout;

export class DefaultPage extends Component {
  static propTypes = {
    layout: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  state  = {
    collapsed: false,
  }

  onCollapse = collapsed => {
    console.log(collapsed)
    this.setState({collapsed})
  }

  render() {
    return (
      <div className="layout-default-page">
        <Layout style={{ minHeight: '100vh' }}>
          <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse}>
            <div className="logo" />
            <Sidebar/>
          </Sider>
          <Layout>
            {/*<Header style={{ padding: 0 }} />*/}
            <Content style={{ margin: '0 16px' }}>
              <ContentHeader/>
              <div className="page-container">{this.props.children}</div>
            </Content>
          </Layout>
        </Layout>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    layout: state.layout,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DefaultPage);
